package fr.ulille.iutinfo.teletp;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class SuiviAdapter extends RecyclerView.Adapter<SuiviAdapter.ViewHolder>/* TODO Q6.a */ {
    // TODO Q6.a
    private SuiviViewModel svm;

    class ViewHolder extends RecyclerView.ViewHolder {
        private final View itemView;

        public void setQuestion(String question) {
            ((TextView) itemView.findViewById(R.id.question)).setText(question);
        }

        public void setChecked(boolean check) {
            ((CheckBox) itemView.findViewById(R.id.checkBox)).setChecked(check);
        }

        public TextView getQuestionView() {
            return itemView.findViewById(R.id.question);
        }

        public CheckBox getCheckBoxView() {
            return itemView.findViewById(R.id.checkBox);
        }


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;

        }
    }

    public  SuiviAdapter(SuiviViewModel svm) {
        this.svm = svm;
    }

    @NonNull
    @Override
    public SuiviAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.question_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SuiviAdapter.ViewHolder holder, int position) {
        String questions = svm.getQuestions(position);
        Integer nxQuestion = svm.getNextQuestion();
        holder.getQuestionView().setText(questions);
        holder.getCheckBoxView().setChecked(nxQuestion > position);
        holder.getCheckBoxView().setEnabled(nxQuestion >= position);
        if (holder.getAdapterPosition() < nxQuestion){
            holder.getCheckBoxView().setOnClickListener(v -> {
                checked(holder.getAdapterPosition());
            });
        } else if (holder.getCheckBoxView().isEnabled()) {
            holder.getCheckBoxView().setOnClickListener(v -> {
                checked(holder.getAdapterPosition() +1);
            });
        }
    }

    @Override
    public int getItemCount() {
        return svm.getAllQuestion().length;
    }

    // TODO Q7
    private void checked(int position) {
        svm.setQuestion(position);
        notifyItemChanged(position);
    }

}
