package fr.ulille.iutinfo.teletp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private Date d;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // TODO Q2.b
        SuiviViewModel model = new ViewModelProvider(this).get(SuiviViewModel.class);
        model.initQuestions(getApplicationContext());

        // TODO Q10
        TextView tvGit = (TextView) findViewById(R.id.tvGit);
        if (tvGit != null) {
            tvGit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Timestamp now = new Timestamp(new Date().getTime());
                    String url = tvGit.getText().toString();
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}