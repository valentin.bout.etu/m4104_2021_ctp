package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class VueListe extends Fragment implements Observer<Integer> {

    // TODO Q2c
    private SuiviViewModel model;
    // TODO Q6
    private SuiviAdapter sv;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_liste, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.btnToGenerale).setOnClickListener(view1 -> NavHostFragment.findNavController(VueListe.this)
                .navigate(R.id.liste_to_generale));

        // TODO Q2c
        model  = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);
        // TODO Q6.b
        RecyclerView rv = (RecyclerView) getActivity().findViewById(R.id.rvQuestions);
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        sv = new SuiviAdapter(model);
        rv.setAdapter(sv);
        // TODO Q7
        LiveData<Integer> liveNQuestions = model.getLiveNextQuestion();
        liveNQuestions.observe(getViewLifecycleOwner(), this);
        // TODO Q8
    }

    @Override
    public void onChanged(Integer pos) {
        sv.notifyDataSetChanged();
    }
}