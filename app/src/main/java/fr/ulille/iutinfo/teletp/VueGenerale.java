package fr.ulille.iutinfo.teletp;

import android.opengl.Visibility;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import org.w3c.dom.Text;

public class VueGenerale extends Fragment implements Observer<Integer> {

    // TODO Q1
    private String salle;
    private String  poste;
    private String DISTANCIEL;
    // TODO Q2.c
    private SuiviViewModel model;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        String[] list_salles = getResources().getStringArray(R.array.list_salles);
        DISTANCIEL = list_salles[0];
        poste = "";
        salle = DISTANCIEL;
        // TODO Q2.c
        model  = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);
        // TODO Q4
        Spinner spSalle = (Spinner) getActivity().findViewById(R.id.spSalle);
        ArrayAdapter<CharSequence> adpaterSpSalle = ArrayAdapter.createFromResource(getContext(), R.array.list_salles, android.R.layout.simple_spinner_dropdown_item);
        adpaterSpSalle.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSalle.setAdapter(adpaterSpSalle);
        Spinner spPoste = (Spinner) getActivity().findViewById(R.id.spPoste);
        ArrayAdapter<CharSequence> adapterSpPoste = ArrayAdapter.createFromResource(getContext(), R.array.list_postes, android.R.layout.simple_spinner_dropdown_item);
        adapterSpPoste.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPoste.setAdapter(adapterSpPoste);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            TextView tvLogin = (TextView) getActivity().findViewById(R.id.tvLogin);
            model.setUsername(tvLogin.getText().toString());
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });


        // TODO Q5.b
        spSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                update();
            }
        });

        spPoste.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                update();
            }
        });

        update();
        // TODO Q9
        TextView tvNextQuestion = (TextView) getActivity().findViewById(R.id.tvNextQuestion);
        tvNextQuestion.setVisibility(View.VISIBLE);
        LiveData<Integer> liveNQuestions = model.getLiveNextQuestion();
        liveNQuestions.observe(getViewLifecycleOwner(), this);

        // TODO Q10
        TextView lbl_git = (TextView) getActivity().findViewById(R.id.lbl_git);
        lbl_git.setVisibility(View.VISIBLE);
        TextView tvgit = (TextView) getActivity().findViewById(R.id.tvGit);
        tvgit.setVisibility(View.VISIBLE);


    }

    // TODO Q5.a
    public void update() {
        Spinner spSalle = (Spinner) getActivity().findViewById(R.id.spSalle);
        salle = spSalle.getSelectedItem().toString();
        if (salle.equals("Distanciel")) {
            Spinner spPoste = (Spinner) getActivity().findViewById(R.id.spPoste);
            spPoste.setVisibility(View.GONE);
            spPoste.setEnabled(false);
            model.setLocalisation("Distanciel");
        } else {
            Spinner spPoste = (Spinner) getActivity().findViewById(R.id.spPoste);
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
            model.setLocalisation(spPoste.getSelectedItem().toString() + " : " + spSalle.getSelectedItem().toString());
        }
    }

    // TODO Q9
    public void onChanged(Integer pos) {
        TextView tvNextQuestion = (TextView) getActivity().findViewById(R.id.tvNextQuestion);
        if (model.getNextQuestion() >= model.getAllQuestion().length) {
            tvNextQuestion.setText(R.string.fini);
        }else {
            tvNextQuestion.setText(model.getQuestions(pos));
        }
    }
}